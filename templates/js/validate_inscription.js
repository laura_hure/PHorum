$(document).ready(function(){

  $("#inscription_form").submit(function (){
    let fail = isValide();

    if(fail == "") return true;
    else{
    //on affiche le message d'erreur et on empêche l'envois du formulaire
    $("#msg_error").html(fail);
    $("#error").show();
    return false;
  }

  });

  function isValide(){
    //verification du pseudo
    let pseudo = $("#pseudo").val();


    if(pseudo.trim() == "") return "Vous devez entrer un pseudo.";
    if(/[!@#\$%\^\&*\)\(+=._-]/.test(pseudo)) return "les caractères spéciaux ne sont pas autorisé.";
    if(pseudo.length >= 60 ) return "Votre pseudo est trop long.";
    if(!/[A-Za-z0-9]{0,60}/.test(pseudo))return "Veuillez entrer un pseudo valide.";

    //verification email
    let email = $("#email").val();
    if(email.trim() == "")return "Vous dever renseigner votre email.";
    if(!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(email)) return "Veuillez entrer une email valide.";

    //verification mot de passe
    let passwd = $("#passwd").val();
    let confirm_passwd = $("#confirm_passwd").val();
    if(passwd != confirm_passwd) return "Les deux mot de passe doivent être identique.";
    if(/[!@#\$%\^\&*\)\(+=._-]/.test(passwd)) return "Les caratères spéciaux ne sont pas autorisé.";
    if(passwd.length < 8) return "Vous devez au minimum entrer 8 caractères pour votre mot de passe.";
    if(passwd.length > 12) return "Vous ne pouvez pas entrer plus de 12 caratères pour votre mot de passe.";
    if(!/[A-Za-z0-9]{8,12}/.test(passwd)) return "Veuillez entrer un mot de passe valide.";

    //il n'y a eu aucun problème
    return "";

  }
});
