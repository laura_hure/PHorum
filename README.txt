Projet PHP dans le cadre de la Licence Professionnel dev web et mobile a l'IUT d'Orl�ans.
Developper un Forum en php.

Installation de la base de données :
  - Modifier les informations user, password, server et nom de la base dans "BD/config.php"
  - Créer la base "forum" dans phpMyAdmin
  - copier/coller la génération des tables de init.sql dans phpMyAdmin

  Installation des packages avec composer :
    - composer install

Lancer le serveur avec composer :
- composer run (php -S localhost:8000 -t web)

Adresse de l'application : http://localhost:8000/PHorum/web/index.php (pas besoin de lancer de serveur)

Pour l'administrateur :
  - pseudo : admin
  - email : admin@admin.fr
  - mot de passe : adminadmin
