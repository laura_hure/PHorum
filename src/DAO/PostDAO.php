<?php

namespace PHorum\DAO;

use \PHorum\Entity\PostEntity;
use \PHorum\BD\Connect;
use \PDO;

/**
* class contenant toutes les méthodes CRUD pour la table POST
*/
class PostDAO{

  //----------------------------------CREATE-------------------------
  /**
  * @param int$utilisateur
  * @param int$sujet
  * @param string$content
  * @return int$id
  */
  public static function createPost(int $utilisateur, int $sujet, string $content){
    try{
    $sql="INSERT INTO post (user,sujet,content,date) VALUES (?,?,?,NOW())";
    $connexion = Connect::getConnexion();
    $query = $connexion->prepare($sql);
    $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
    $query->bindParam(2,$sujet,PDO::PARAM_INT);
    $query->bindParam(3,$content,PDO::PARAM_STR,500);
    $res = $query->execute();
  }catch(PDOException $e){
    echo "PostDAO : createPost : erreur lors de la creation du post".$e->getMessage();
    exit;
  }

    return $connexion->lastInsertId();
  }

  /**
  * @param int$utilisateur
  * @param int$sujet
  * @param string$content
  * @param DateTime$date
  * @return int$id
  */
  public static function createPostWithDate(int $utilisateur, int $sujet, string $content,$date){
    try{
    $sql="INSERT INTO post (user,sujet,content,date) VALUES (?,?,?,?)";
    $connexion = Connect::getConnexion();
    $query = $connexion->prepare($sql);
    $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
    $query->bindParam(2,$sujet,PDO::PARAM_INT);
    $query->bindParam(3,$content,PDO::PARAM_STR,500);
    $query->bindParam(4,$date);
    $res = $query->execute();
  }catch(PDOException $e){
    echo "PostDAO : createPost : erreur lors de la creation du post".$e->getMessage();
    exit;
  }

    return $connexion->lastInsertId();
  }

  //----------------------------------UPDATE-----------------------------
  /**
  * @param int$id
  * @param string$new_content
  */
  public static function updatePostContent(int $id,string $new_content){
    $sql="UPDATE post SET content=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$new_content,PDO::PARAM_STR,500);
    $query->bindParam(2,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("PostDAO : updatePostContent : erreur lors de la mise à jour du post");
  }

  //---------------------------DELETE----------------------------
  /**
  * @param int$id
  */
  public static function deletePostById(int $id){
    try{$sql="DELETE FROM post where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "PostDAO : deletePostById : erreur lors de la suppression de ce post : " . $e->getMessage();
    exit;
  }

  }

  /**
  * @param int$sujet
  */
  public static function deletePostBySujet(int $sujet){
    try{
    $sql="DELETE FROM post where sujet=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$sujet,PDO::PARAM_INT);
    $res = $query->execute();
  }catch(PDOException $e){
    echo "PostDAO : deletePostBySujet :erreur lors de la suppression des post de ce sujet : ".$e->getMessage();
    exit;
  }
  }

  /**
  * @param int$utilisateur
  */
  public static function deletePostByUtilisateur(int $utlisateur){
    $sql="DELETE FROM post where user=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
    $res = $query-execute();

    if(!$res)die("PostDAO : deletePostByUtilisateur : erreur lors de la suppression des post de cet utilisateur");
  }

  //------------------------READ--------------------
  /**
  * @return array$posts
  */
  public static function getAllPosts(){
    try{$sql = "SELECT * from post";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "PosteDAO : getAllPosts : erreur lors de la récupération des posts : " . $e->getMessage();
    exit;
  }

    $posts = array();
    foreach($query as $post){
      $posts[$post["id"]] = new PostEntity(array(
        "id" => $post["id"],
        "sujet" => $post["sujet"],
        "utilisateur" => $post["user"],
        "content" => $post["content"]
      ));
    }
    return $posts;
  }

  /**
  * @param int$id
  * @return EntityPost$post
  */
  public static function getPostById(int $id){
    $sql="SELECT * from post where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("PostDAO : getPostById : erreur lors de la recuperation du post ayant pour id ".$id);

    $post = null;
    foreach($query as $p){
      $post = new PostEntity(array(
        "id" => $p["id"],
        "sujet" => $p["sujet"],
        "utilisateur" => $p["user"],
        "content" => $p["content"]
      ));
    }
    return $post;
  }

  /**
  * @param int$sujet
  * @return array$posts
  */
  public static function getPostsBySujet(int $sujet){
    try{$sql="SELECT id, sujet, user, content, DATE_FORMAT(date, '%d/%m/%Y %Hh%imin%ss') AS date from post where sujet=? ORDER BY date";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$sujet,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "PostDAO : getPostBySujet : erreur lors de la récupération des posts de ce sujet : " . $e->getMessage();
    exit;
  }

    $posts = array();
    foreach($query as $post){
      $posts[$post["id"]] = new PostEntity(array(
        "id" => $post["id"],
        "sujet" => $post["sujet"],
        "utilisateur" => $post["user"],
        "content" => $post["content"],
        "date" => $post["date"]
      ));
    }
    return $posts;
  }

  /**
  * @param int$utilisateur
  * @return array$posts
  */
  public static function getPostsByUtilisateur(int $utilisateur){
    $sql="SELECT * from post where user=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("PostDAO : getPostsByUtilisateur : erreur lors de la récupération des posts de cet utilisateur");

    $post = array();
    foreach($query as $post){
      $posts[$post["id"]] = new PostEntity(array(
        "id" => $post["id"],
        "sujet" => $post["sujet"],
        "utilisateur" => $post["user"],
        "content" => $post["content"]
      ));
    }
    return $posts;
  }
}

 ?>
