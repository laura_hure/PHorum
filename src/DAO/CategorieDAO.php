<?php

namespace PHorum\DAO;

use \PHorum\Entity\CategorieEntity;
use \PHorum\BD\Connect;
use \PDO;


class CategorieDAO{

  //-------------------------CREATE-----------------
  /**
  * @param string$titre
  * @return int$id
  */
  public static function createCategorie(string $titre){
    try{$sql="INSERT INTO categorie (titre) VALUES (?)";
    $connexion = Connect::getConnexion();
    $query = $connexion->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "CategorieDAO : createCategorie : erreur lors de la creation de la catgéorie : " . $e->getMessage();
    exit;
  }

    return $connexion->lastInsertId();
  }

  //--------------------DELETE---------------------
  /**
  * @param int$id
  */
  public static function deleteCategorieById(int $id){
  try{  $sql="DELETE FROM categorie where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "Categorie : deleteById : erreur lors de la suppression de la categorie : " . $e->getMessage();
    exit;
  }

  }

  /**
  * @param string$titre
  */
  public static function deleteByTitre(string $titre){
    $sql="DELETE FROM categorie where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : deleteByTitre : erreur lors de la suppression de la catégorie ayant pour titre ".$titre);
  }

  //-----------------------------------UPDATE-----------------------
  /**
  * @param int$id
  * @param string$new_titre
  */
  public static function updateCategorieTitre(int $id, string $new_titre){
    try{$sql="UPDATE categorie SET titre=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$new_titre,PDO::PARAM_STR,60);
    $query->bindParam(2,$id,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "CategorieDAO : updateCategorieTitre : erreur lors de la mise à jour de la categorie : " . $e->getMessage();
    exit;
  }
  }

  //------------------------------------READ---------------------------
  /**
  * @return array$categories
  */
  public static function getAllCategories(){
  try{  $sql="SELECT * FROM categorie";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "CategoriDAO : getAllCategories : erreur lors de la mise la recuperation de toutes les categories : " . $e->getMessage();
    exit;
  }

    $categories = array();
    foreach($query as $categorie){
      $categories[$categorie["id"]] = new CategorieEntity(array(
        "id" => $categorie["id"],
        "titre" => $categorie["titre"]
      ));
    }
    return $categories;
  }

  /**
  * @param int$id
  * @return CategorieEntity$categorie
  */
  public static function getCategorieById(int $id){
    $sql="SELECT * FROM categorie where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : getCategorieById : erreur lors de la recuperation de la categorie ayant pour id ".$id);

    $categorie = null;
    foreach($query as $c){
      $categorie = new CategorieEntity(array(
        "id" => $c["id"],
        "titre" => $c["titre"]
      ));
    }
    return $categorie;
  }

  /**
  * @param string$titre
  * @return CategorieEntity$categorie
  */
  public static function getCategorieByTitre(string $titre){
    $sql="SELECT * FROM categorie where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("CategorieDAO : getCategorieTitre : erreur lors de la recuperation de la categorie ayant pour titre ".$titre);

    $categorie = null;
    foreach($query as $c){
      $categorie = new CategorieEntity(array(
        "id" => $c["id"],
        "titre" => $c["titre"]
      ));
    }
    return $categorie;
  }
}
 ?>
