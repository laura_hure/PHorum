<?php

namespace PHorum\DAO;


//----------------Import-----------------
use \PHorum\Entity\UtilisateurEntity;
use \PHorum\BD\Connect;
use \PDO;


/**
* Data Access Object des utilisateurs
*/
class UtilisateurDAO{
  //------------READ------------

  /**
  * @return array$utilisateurs tableau d'utilisateur
  */
  public static function getAllUtilisateurs(){
    try{$sql = "SELECT * from utilisateur";
    $query =  Connect::getConnexion()->prepare($sql);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : getAllUtilisateurs : erreur lors de la recuperation des utilisateurs : " . $e->getMessage();
    exit;
  }

    $users = array();
    foreach($query as $user){
      $users[$user["id"]]=new UtilisateurEntity(array(
        "id" => $user["id"],
        "pseudo" => $user["pseudo"],
        "mail" => $user["email"],
        "type" => $user["type"],
        "passwd" => $user["passwd"]
      ));
    }
    return $users;
  }

  /**
  * @param int$id
  * @return UtilisateurEntity$utilisateur
  */
  public static function getUtilisateurById(int $id){
    try{$sql = "SELECT * from utilisateur where id=?";
    $query =  Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : getUtilisateurById : erreur lors de la recuperation de l'utilisateur : " . $e->getMessage();
    exit;
  }

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"],
        "passwd" => $u["passwd"]
      ));
    }

    return $user;
  }

  /**
  * @param string$pseudo
  * @return UtilisateurEntity$Utilisateur utilisateur qui possède ce pseudo
  */
  public static function getUtilisateurByPseudo(string $pseudo){
    try{$sql = "SELECT * from utilisateur where pseudo=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$pseudo,PDO::PARAM_STR,20);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : getUtilisateurByPseudo : erreur lors de la recuperation de l'utilisateur : " . $e->getMessage();
    exit;
  }

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"],
        "passwd" => $u["passwd"]
      ));
    }

    return $user;
  }

  /**
  * @param string$mail
  * @return UtilisateurEntity$Utilisateur utilisateur qui possède ce mail
  */
  public static function getUtilisateurByMail(string $mail){
    try{$sql = "SELECT * from utilisateur where email=?";
    $query =  Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$mail,PDO::PARAM_STR,50);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : getUtilisateurByMail : erreur lors de la recuperation de l'utilisateur : " . $e->getMessage();
    exit;
  }

    $user = null;
    foreach($query as $u){
      $user = new UtilisateurEntity(array(
        "id" => $u["id"],
        "pseudo" => $u["pseudo"],
        "mail" => $u["email"],
        "type" => $u["type"],
        "passwd" => $u["passwd"]
      ));
    }

    return $user;
  }

  /**
  * @param int$type
  * @return array$utilisateurs liste des utilisateurs qui ont se type
  */
  public static function getUtilisateursByType(int $type){
    $sql = "SELECT * from utilisateur where type=?";
    $query =  $this->getConnexion()->prepare($sql);
    $query->bindParam(1,$type,PDO::PARAM_STR,10);
    $res = $query->execute();

    if(!$res)di("UtilisateurDAO : getUtilisateurByType : erreur lors de la recuperation des utilisateurs d'on le type est ".$type);

    $users = array();
    foreach($query as $user){
      $users[$user["id"]] = new UtilisateurEntity(array(
        "id" => $user["id"],
        "pseudo" => $user["pseudo"],
        "mail" => $user["email"],
        "type" => $user["type"]
      ));
    }
    return $users;
  }

  //---------------------CREATE----------------
  /**
  * @param string$pseudo
  * @param string$mail
  * @param int$type
  * @param string$passwd
  * @return int$id
  */
  public static function createUtilisateur(string $pseudo, string $mail, int $type,string $passwd){
    try{$sql = "INSERT INTO utilisateur (pseudo,email,type,passwd) VALUES (?,?,?,?)";
    $connexion =  Connect::getConnexion();
    $query = $connexion->prepare($sql);

    $query->bindParam(1,$pseudo,PDO::PARAM_STR,60);
    $query->bindParam(2,$mail,PDO::PARAM_STR,50);
    $query->bindParam(3,$type,PDO::PARAM_INT);
    $query->bindParam(4,$passwd,PDO::PARAM_STR,255);

    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : createUtilisateur : erreur lors de la creation de l'utilisateur : " . $e->getMessage();
    exit;
  }

    return $connexion->lastInsertId();
  }

  //--------------------UPDATE----------------
  /**
  * @param int$id
  * @param string$new_mail
  */
  public static function updateUtilisateurMail(int $id, string $new_mail){
    $sql = "UPDATE utilisateur SET email=? where id=?";
    $query =  $this->getConnexion()->prepare($sql);

    $query->bindParam(1,$new_mail,PDO::PARAM_STR,50);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurMail : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau mail : ".$new_mail);
  }

  /**
  * @param int$id
  * @param string$new_pseudo
  */
  public static function updateUtilisateurPseudo(int $id, string $new_pseudo){
    $sql = "UPDATE utilisateur SET pseudo=? where id=?";
    $query =  $this->getConnexion()->prepare($sql);

    $query->bindParam(1,$new_pseudo,PDO::PARAM_STR,20);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurPseudo : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau speudo :".$new_pseudo);
  }

  /**
  * @param int$id
  * @param int$new_type
  */
  public static function updateUtilisateurType(int $id, int $new_type){
    $sql = "UPDATE utilisateur SET type=? where id=?";
    $query = $this->getConnexion()->prepare($sql);

    $query->bindParam(1,$new_type,PDO::PARAM_INT);
    $query->bindParam(2,$id,PDO::PARAM_INT);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : updateUtilisateurType : erreur lors de la mise à jour de l'utilisateur ayant pour id : ".$id." avec comme nouveau type : ".$type);

  }

  //-----------------------DELETE-------------------
  /**
  * @param int$id
  */
  public static function deleteUtilisateurById(int $id){
    try{$sql = "DELETE FROM utilisateur where id=?";
    $query =  Connect::getConnexion()->prepare($sql);

    $query->bindParam(1,$id,PDO::PARAM_INT);

    $res = $query->execute();
  }catch (PDOException $e) {
    echo "UtilisateurDAO : deleteUtilisateurById : erreur lors de la suppression de l'utilisateur : " . $e->getMessage();
    exit;
  }

  }

  /**
  *@param string$pseudo
  */
  public static function deleteUtilisateurByPseudo(string $pseudo){
    $sql = "DELETE FROM utilisateur where pseudo=?";
    $query =  $this->getConnexion()->prepare($sql);

    $query->bindParam(1,$pseudo,PDO::PARAM_STR,30);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : deleteUtilisateurByPseudo : erreur lors de le suppression de l'utilisateur ayant pour speudo : ".$pseudo);
  }

  /**
  *@param string$mail
  */
  public static function deleteUtilisateurByMail(string $mail){
    $sql = "DELETE FROM utilisateur where email=?";
    $query =  $this->getConnexion()->prepare($sql);

    $query->bindParam(1,$mail,PDO::PARAM_STR,60);

    $res = $query->execute();
    if(!$res)die("UtilisateurDAO : deleteUtilisateurByMail : erreur lors de la suppression de l'utilisateur ayant pour mail : ".$mail);
  }


}
 ?>
