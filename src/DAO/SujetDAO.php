<?php

namespace PHorum\DAO;

use \PHorum\Entity\SujetEntity;
use \PHorum\BD\Connect;
use \PDO;

/**
* methodes CRUD pour la table sujet
*/
class SujetDAO{

  //-----------------------CREATE---------------------------
  /**
  * @param string$titre
  * @param int$utilisateur
  * @param int$categorie
  * @return int$id id du sujet qui vient d'etre cree
  */
  public static function createSujet(string $titre, int $utilisateur, int $categorie){
    try{$sql="INSERT INTO sujet (titre,user,categorie) VALUES (?,?,?)";
    $connexion = Connect::getConnexion();
    $query =  $connexion->prepare($sql);

    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $query->bindParam(2,$utilisateur,PDO::PARAM_INT);
    $query->bindParam(3,$categorie,PDO::PARAM_INT);

    $res = $query->execute();
  }catch (PDOException $e) {
    echo "SujetDAO : createSujet : erreur lors de l'insertion du sujet : " . $e->getMessage();
    exit;
  }

    return $connexion->lastInsertId();
  }

  //------------------------READ-----------------------------
  /**
  * @return array$sujets
  */
  public static function getAllSujets(){
    try{$sql="SELECT * from sujet";
    $query = Connect::getConnexion()->prepare($sql);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "SujetDAO : getAllSujets : erreur lors de la recuperation de tous les sujets : " . $e->getMessage();
    exit;
  }

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]]= new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  /**
  * @param int$id
  * @return SujetEntity$sujet
  */
  public static function getSujetById(int $id){
    $sql="SELECT * from sujet where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("SujetDAO : getSujetById : erreur lors de la récupération du sujet ayant pour id : ".$id);

    $sujet = null;
    foreach($query as $s){
      $sujet = new SujetEntity(array(
        "id" => $s["id"],
        "titre" => $s["titre"],
        "utilisateur" => $s["user"],
        "categorie" => $s["categorie"]
      ));
    }
    return $sujet;
  }

  /**
  * @param string$titre
  * @return array$sujets
  */
  public static function getSujetsByTitre(string $titre){
    $sql="SELECT * from sujet where titre=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $res = $query->execute();

    if(!$res)die("SujetDAO : getSujetsByTitre : erreur de la recuperation des sujet ayant pour titre : ".$titre);

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]] = new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  /**
  * @param int$utilisateur
  * @return array$sujets
  */
  public static function getSujetsByUtilisateur(int $utilisateur){
    $sql="SELECT * from sujet where user=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(14,$utilisateur,PDO::PARAM_INT).
    $res = $query->execute();

    if(!$res)di("SujetDAO : getSujetsByUtilisateur : erreur de la recuperation du sujet ayant pour id d'utilisateur : ".$utilisateur);

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]] = new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  /**
  * @param int$categorie
  * @return array$sujets
  */
  public static function getSujetsByCategorie(int $categorie){
    try{$sql="SELECT * FROM sujet where categorie=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$categorie,PDO::PARAM_INT);
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "SujetDAO : getSujetsByCategorie : erreur lors de la recuperation des sujets appartenant à la catégorie : " . $e->getMessage();
    exit;
  }

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]] = new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  /**
   * @param int$categorie
   * @param int$utilisateur
   * @return array$sujets
   * retourne la liste des sujets qui appartiennent à cette categorie ET utilisateur
   */
  public static function getSujetByUtilisateurAndCategorie(int $categorie, int $utilisateur){
    try{
      $sql = "SELECT * from sujet WHERE categorie=? and user=?";
      $query = Connect::getConnexion()->prepare($sql);

      $query->bindParam(1,$categorie,PDO::PARAM_INT);
      $query->bindParam(2,$utilisateur,PDO::PARAM_INT);

      $res = $query->execute();
    }catch(PDOException $e){
      echo "SujetDAO : getSujetByUtilisateurAndCategorie : erreur lors de la recuperation des sujets :". $e->getMessage();
      exit;
    }

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]] = new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  /**
  * @param int$categorie
  * @param int$utilisateur
  * @return array$sujets
  * retourne la liste des sujets appartenant à cette categorie mais pas à cet utilisateur
  */
  public static function getSujetsByCategorieSansUser(int $categorie, int $utilisateur){
    try{
      $sql = "SELECT * from sujet where user !=? and categorie=?";
      $query = Connect::getConnexion()->prepare($sql);

      $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
      $query->bindParam(2,$categorie,PDO::PARAM_INT);

      $query->execute();
    }catch(PDOException $e){
      echo "SujetDAO : getSujetByCategorieSansUser : erreur lors de la recuperation des sujets : ".$e->getMessage();
      exit;
    }

    $sujets = array();
    foreach($query as $sujet){
      $sujets[$sujet["id"]] = new SujetEntity(array(
        "id" => $sujet["id"],
        "titre" => $sujet["titre"],
        "utilisateur" => $sujet["user"],
        "categorie" => $sujet["categorie"]
      ));
    }
    return $sujets;
  }

  //------------------------------------DELETE------------------------
  /**
  * @param int$id
  */
  public static function deleteSujetById(int $id){
    try{$sql="DELETE FROM sujet where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$id,PDO::PARAM_INT).
    $res = $query->execute();
  }catch (PDOException $e) {
    echo "SujetDAO : deleteSujetById : erreur lors de la suppression du sujet : " . $e->getMessage();
    exit;
  }
  }

  /**
  * @param int$utilisateur
  */
  public static function deleteByUtilisateur(int $utilisateur){
    $sql="DELETE FROM sujet where user=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$utilisateur,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("SujetDAO : deleteByUtilisateur : erreur lors de la suppresion des sujet de cet utilisateur");
  }

  //-----------------------------------UPDATE---------------------------------
  /**
  * @param
  * @param string$new_titre
  */
  public static function updateSujetTitre(int $id,string $titre){
    $sql="UPDATE sujet SET titre=? where id=?";
    $query = Connect::getConnexion()->prepare($sql);
    $query->bindParam(1,$titre,PDO::PARAM_STR,60);
    $query->bindParam(2,$id,PDO::PARAM_INT);
    $res = $query->execute();

    if(!$res)die("SujetDAO : updateSujetTitre : erreur lors de la mise à jour du titre du sujet ayant pour id : ".$id);
  }

}
 ?>
