<?php

namespace PHorum\Utils;

//Les traits sont un mécanisme de réutilisation de code en autorisant le développeur
//à réutiliser un certain nombre de méthodes dans des classes indépendantes
trait SetUtils{
   public function setUtils(array $donnees){
     //On parcours le tableau de façon à avoir une clé associé à une valeur
     foreach ($donnees as $key => $value){
       //On construit le nom de la méthode set + $key
       $method = 'set'.ucfirst(strtolower($key));
       //Vérifie que la tableau contient bien un objet et un nom de méthode
       if(is_callable([$this,$method])){
         //On appelle le setter de l'objet et on lui donne la value
         $this->$method($value);
       }
    }
  }
}
 ?>
