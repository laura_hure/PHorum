<?php

namespace PHorum\Services;

use \PHorum\DAO\UtilisateurDAO;

class LoginService{

  /**
  * @return boolean
  */
  public static function CheckFields($pseudo,$email,$passwd){
      if($pseudo == "") return "vous devez renseigner votre pseudo.";
      if($email == "") return "Vous devez renseigner votre email.";
      if($passwd == "") return "Vous devez renseigner votre mot de passe.";
      if(preg_match("/[^A-Za-z0-9]/",$pseudo) && strlen($pseudo) > 60) return "Votre pseudo ne respecte pas le format requis.";
      if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) return "Votre email ne respecte pas le format requis.";
      if(preg_match("/[^A-Za-z0-9]/",$passwd) && strlen($passwd) < 8) return "Votre mot de passe ne respecte pas le format requis.";
      if(UtilisateurDAO::getUtilisateurByPseudo($pseudo) == null) return "Ce pseudo n'existe pas.";
      if(UtilisateurDAO::getUtilisateurByMail($email) == null) return "Cet email n'existe pas.";
      if(UtilisateurDAO::getUtilisateurByPseudo($pseudo)->getMail() != $email) return "Cet email est incorrecte";
      return "";
  }
}
