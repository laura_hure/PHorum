<?php

namespace PHorum\Services;

use \PHorum\DAO\UtilisateurDAO;
use \PHorum\DAO\TypeDAO;

class InscriptionService{

  /**
  * @return boolean
  */
  public static function CheckFields($pseudo,$email,$passwd,$confirm_passwd){
      if($pseudo == "") return "Vous devez renseigner votre pseudo.";
      if($email == "") return "Vous devez renseigner votre email.";
      if($passwd == "") return "Vous devez renseigner votre mot de passe.";
      if($confirm_passwd == "") return "Vous devez confirmer votre mot de passe.";
      if(UtilisateurDAO::getUtilisateurByPseudo($pseudo) != null) return "Ce pseudo est déjà utilisé.";
      if(preg_match("/[^A-Za-z0-9]/",$pseudo) && strlen($pseudo) > 60) return "Votre pseudo ne respecte pas le format requis.";
      if(UtilisateurDAO::getUtilisateurByMail($email) != null) return "Cet email est déjà utilisé.";
      if(filter_var($email, FILTER_VALIDATE_EMAIL) == false) return "Votre email ne respecte pas le format requis.";
      if($passwd != $confirm_passwd) return "Votre mot de passe et sa confirmation doivent être identiques.";
      if(preg_match("/[^A-Za-z0-9]/",$passwd) && strlen($passwd) < 8) return "Votre mot de passe ne respecte pas le format requis.";
      return "";
  }

  public function inscrire($pseudo,$email,$passwd){
    $cryptedPass = password_hash($passwd, PASSWORD_DEFAULT);
    return UtilisateurDAO::createUtilisateur($pseudo,$email,TypeDAO::getTypeByType("membre")->getId(),$cryptedPass);
  }
}
 ?>
