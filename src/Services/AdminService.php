<?php
namespace PHorum\Services;

use \PHorum\DAO\SujetDAO;
use \PHorum\DAO\PostDAO;
use \PHorum\DAO\CategorieDAO;
use \PHorum\DAO\UtilisateurDAO;

use \DomDocument;
use \ZipArchive;
use \DOMImplementation;
use \DateTime;

class AdminService{


  /**
  * @param int$id
  * supprime tous le sujets et post lié à la catégorie
  */
  public static function deleteCategorie(int $id){

    //recuperation de sujets liés à la categorie
    $sujets = SujetDAO::getSujetsByCategorie($id);

    foreach($sujets as $sujet){
      //recuperation et des posts liés à ces sujets
      $posts = PostDAO::getPostsBySujet($sujet->getId());
      //suppression des posts
      foreach($posts as $post){
        PostDAO::deletePostById($post->getId());
        }
        //suppression du sujet
        SujetDAO::deleteSujetById($sujet->getId());
      }

    //suppression de la categorie
    CategorieDAO::deleteCategorieById($id);

  }

  /**
  * @param int$id
  * genere un fichier XML et JSON avec tous les sujets et posts lie a la categorie
  */
  public static function downloadCategorie(int $id){

    // Creates an instance of the DOMImplementation class
    $imp = new DOMImplementation;

    // Creation de la DTD
    $dtd = $imp->createDocumentType("categorie", '', 'http://localhost/PHorum/resources/import.dtd');

    //preparation de l'xml
    $xml = $imp->createDocument("", "", $dtd);
    $json = array();

    //creation de la categorie
    $titre_categorie = CategorieDAO::getCategorieById($id)->getTitre();
    $categorie = $xml->createElement("categorie");
    $categorie->setAttribute("titre",$titre_categorie);
    $json[$titre_categorie]=array();

    //creation et recuperation des sujets lies a la categorie
    //$listSujets = $xml->createElement("sujets");
    $sujets = SujetDAO::getSujetsByCategorie($id);

    foreach($sujets as $sujet){
      $titre_sujet = $sujet->getTitre();
      $elem_sujet = $xml->createElement("sujet");
      $elem_sujet->setAttribute("titre",$titre_sujet);

      //recuperation des posts lies au sujet
      $posts = PostDAO::getPostsBySujet($sujet->getId());
      //$listPosts = $xml->createElement("posts");
      foreach($posts as $post){
        //creation du post ayant pour attribut l'utilisateur et le contenue en tant qu'enfant
        $elem_post = $xml->createElement("post");
        $pseudo_utilisateur = UtilisateurDAO::getUtilisateurById($post->getUtilisateur())->getPseudo();
        $elem_post->setAttribute("utilisateur",$pseudo_utilisateur);
        $elem_post->setAttribute("date",$post->getDate());
        $text = $xml->createTextNode($post->getContent());
        $elem_post->appendChild($text);

        //on ajoute le post a la liste des posts
        //$listPosts->appendChild($elem_post);

        //creation de l'attribut utilisateur pour le sujet
        $elem_sujet->setAttribute("utilisateur",$pseudo_utilisateur);

        $json[$titre_categorie][$titre_sujet]=array("utilisateur" => $pseudo_utilisateur);

        $json[$titre_categorie][$titre_sujet][$post->getId()]=array(
          "utilisateur" => $pseudo_utilisateur,
          "contenu" => $post->getContent(),
          "date" => $post->getDate()
        );
        $elem_sujet->appendChild($elem_post);
      }

      //on ajoute la liste des postes au sujet et le sujet a la liste des sujets
      //$elem_sujet->appendChild($listPosts);
      //$listSujets->appendChild($elem_sujet);
      $categorie->appendChild($elem_sujet);
    }

    //on ajoute la liste de sujets à la categorie et la categorie au document xml
    //$categorie->appendChild($listSujets);
    $xml->appendChild($categorie);

    //sauvegarde de l'xml
    $xml->save("../resources/exports/export.xml");

    //sauvegarde du json
    $fp = fopen('../resources/exports/export.json', 'w');
    fwrite($fp, json_encode($json));
    fclose($fp);

    //generation de l'archive zip
    $zip = new ZipArchive();
    if($zip->open('../resources/exports.zip', ZipArchive::CREATE) == TRUE){
      // Récupération des fichiers.
	    $fichiers = scandir('../resources/exports/');
	    // On enlève . et .. qui représentent le dossier courant et le dossier parent.
	     unset($fichiers[0], $fichiers[1]);

	      foreach($fichiers as $f){
	          // On ajoute chaque fichier à l’archive en spécifiant l’argument optionnel.
	          // Pour ne pas créer de dossier dans l’archive.
	          $zip->addFile('../resources/exports/'.$f, $f);

	         }
    }
    $zip->close();

    //on lance le téléchargement
    AdminService::downloadFile("../resources/exports.zip");
  }

  /**
  * prend en paramètre un fichier et permet sont telechargement
  */
  private static function downloadFile($file) { // $file = include path
        if(file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
}

  public static function import($file){
    //verifie que le fichier a bien charge
  if ($file['error'] > 0) return "Erreur lors du transfert du fichier.";

  //recuperation de l'extension et verification que c'est bien un xml
  $extension_upload = strtolower(  substr(  strrchr($file['name'], '.')  ,1)  );
  if ( $extension_upload != "xml" ) return "Extension incorrecte.";

  //creation du DOM et chargement du fichier
  $xml = new DOMDocument;
  $xml->load($file["tmp_name"]);
  if(!$xml->validate()){
      return "Le fichier n'est pas conforme.";
  }

  //si y a une categorie
  $categorie = $xml->getElementsByTagName("categorie");
  foreach($categorie as $cat){
    $categories = CategorieDAO::getCategorieByTitre($cat->attributes->getNamedItem("titre")->nodeValue);
      if($categories){
        $id_categorie = $categories->getId();
      }else{
        $id_categorie = CategorieDAO::createCategorie($cat->attributes->getNamedItem("titre")->nodeValue);
      }

      foreach($cat->childNodes as $sujet){
        //si le node est bien un xml element on le traite si non on passe au suivant
        if ($sujet->nodeType == XML_ELEMENT_NODE){
          $utilisateur = UtilisateurDAO::getUtilisateurByPseudo($sujet->attributes->getNamedItem("utilisateur")->nodeValue);

          //si l'utilisateur existe on ajoute le sujet
          if($utilisateur){
            $id_sujet = SujetDAO::createSujet($sujet->attributes->getNamedItem("titre")->nodeValue,$utilisateur->getId(),$id_categorie);

            //parcourt des posts de se sujet
            foreach($sujet->childNodes as $post){
              if ($post->nodeType == XML_ELEMENT_NODE){
                $utilisateur_post = UtilisateurDAO::getUtilisateurByPseudo($post->attributes->getNamedItem("utilisateur")->nodeValue);
              if($utilisateur_post){
                  $date = $post->attributes->getNamedItem("date")->nodeValue;
                  $id_post = PostDAO::createPostWithDate($utilisateur_post->getId(),$id_sujet,$post->nodeValue,strtotime($date));
              }else{
                return "Certains utilisateurs n'existent pas.";
              }
            }
          }
          }else{
            return "Certains utilisateurs n'existent pas.";
          }
        }
      }
  }
  return "";
  }
}
?>
