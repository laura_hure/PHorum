<?php
namespace PHorum\Services;

use \PHorum\DAO\SujetDAO;
use \PHorum\DAO\UtilisateurDAO;
use \PHorum\DAO\PostDAO;

class ForumService{

  /**
  * @param string$sujet
  * @return boolean$valide
  * vérifie le sujet avant de l'ajouter
  */
  public static function checkSujet(string $sujet){
    if($sujet == "" || $sujet.trim() == "") return false;
    if(preg_match("/[^A-Za-z0-9]/",$sujet) && strlen($sujet) > 60) return false;
    return true;
  }

  /**
  * @param array$sujets
  * @return array$users
  * retourne la liste des utilisateur associé à ces sujets
  */
  public static function getUserForSujet(array $sujets){
    $users = array();

    foreach($sujets as $sujet){
      $user = UtilisateurDAO::getUtilisateurById($sujet->getUtilisateur());
      $users[$user->getId()] = $user;
    }

    return $users;
  }

  /**
  * @param int$sujetId
  * prend en parametre l'identifiant du sujet à supprimer et supprime le sujet ainsi que tout les posts associé
  */
  public static function deleteSujetAndPosts(int $sujetId){
    PostDAO::deletePostBySujet($sujetId);
    SujetDAO::deleteSujetById($sujetId);
  }

  /**
  * @param array$posts
  * @return array$users
  * prend en argument une liste de post et en retourne les utilisateur associes
  */
  public static function getUserForPost(array $posts){
    $users = array();

    foreach($posts as $post){
      $user = UtilisateurDAO::getUtilisateurById($post->getUtilisateur());
      $users[$user->getId()] = $user;
    }

    return $users;
  }

  /**
  * prend en paramètre une image et l'enregitre dans le système de fichier du serveur
  * @param image
  * @param string$file_name
  * @return string$erreur_message
  */
  public static function uploadImage($image,$file_name){
      $target_dir = "C:\\xampp\\htdocs\\PHorum\\resources\\images\\";
      $target_file = $target_dir . basename($image["name"]);
      $uploadOk = 1;
      $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

      // Check if image file is a actual image or fake image
      $check = getimagesize($image["tmp_name"]);
      if($check !== false) {
        $uploadOk = 1;
        } else {
          return "Ce fichier n'est pas une image.";
          $uploadOk = 0;
        }
        // Check file size
        if ($image["size"] > 500000) {
          return "Le fichier est trop lourd.";
          $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg") {
          return "Seul le format JPG est autorisé.";
          $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
          return "Une erreur est survenu lors du téléchargement.";
          // if everything is ok, try to upload file
        } else {
          if (move_uploaded_file($image["tmp_name"], $target_dir.$file_name.".".$imageFileType)) {
            return "";
          } else {
            return "Une erreur est survenu lors du téléchargement.";
          }
        }

      }

      /**
      * indique si l'image existe ou non
      * @param string$image
      * @return boolean$exist
      */
      public static function imageExist($image){
        if (file_exists($image)) {
            return true;
          }
        return false;
      }
}
 ?>
