CREATE DATABASE FORUM;

CREATE TABLE TYPE(
	id int NOT NULL AUTO_INCREMENT,
	type VARCHAR(30) UNIQUE,
	PRIMARY KEY (id)
);

CREATE TABLE UTILISATEUR(
	id int NOT NULL AUTO_INCREMENT,
	pseudo VARCHAR(60) NOT NULL UNIQUE,
	email VARCHAR(60) NOT NULL UNIQUE,
	passwd VARCHAR(255) NOT NULL,
	type int NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (type) REFERENCES TYPE(id)
);

CREATE TABLE CATEGORIE(
	id int NOT NULL AUTO_INCREMENT,
	titre VARCHAR(60) UNIQUE,
	PRIMARY KEY(id)
);

CREATE TABLE SUJET(
	id int NOT NULL AUTO_INCREMENT,
	titre VARCHAR(60),
	user int NOT NULL,
	categorie int NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (user) REFERENCES UTILISATEUR(id),
	FOREIGN KEY (categorie) REFERENCES CATEGORIE(id)
);

CREATE TABLE POST(
	id int NOT NULL AUTO_INCREMENT,
	user int NOT NULL,
	sujet int NOT NULL,
	content VARCHAR(500),
	date DATETIME,
	PRIMARY KEY (id),
	FOREIGN KEY (user) REFERENCES UTILISATEUR(id),
	FOREIGN KEY (sujet) REFERENCES SUJET(id)
);

//------------------TYPE------------------
INSERT INTO `type` (`type`) VALUES ("administrateur");
INSERT INTO `type` (`type`) VALUES ("membre");

//---------------------ADMIN---------------
INSERT INTO `utilisateur` (`pseudo`,`email`,`passwd`,`type`) VALUES ("admin","admin@admin.fr","$2y$10$GjN/r1Lqn0hJnJqeIvWPsuT6s2Jxz0EKEXFIUo5fWRpoJF8Fs23j.",1);

//--------------------CATEGORIES--------------------
INSERT INTO `categorie` (`titre`) VALUES("Overwatch");
INSERT INTO `categorie` (`titre`) VALUES("Hearsthone");
INSERT INTO `categorie` (`titre`) VALUES("Heros of the storm");
INSERT INTO `categorie` (`titre`) VALUES("League of legend");
