<?php
namespace PHorum\BD;
use \PDO;
require_once("config.php");


class Connect{
public static function getConnexion(){
  // pour oracle: $dsn="oci:dbname=//serveur:1521/mydb
  $dsn="mysql:dbname=".BASE.";host=".SERVER;
    try{
      $connexion=new PDO($dsn,USER,PASSWD, array(PDO::ATTR_PERSISTENT =>true));
    }
    catch(PDOException $e){
      printf("Échec de la connexion : %s\n", $e->getMessage());
      exit();
    }

    return $connexion;
  }
}

?>
