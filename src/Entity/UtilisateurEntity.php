<?php
namespace PHorum\Entity;
//require_once("../Utils/SetUtils.php");

/**
* Classe représentant la table utilisateur de la BASE
* elle est uniquement destinée à transporter les données
*/
class UtilisateurEntity{

  use \PHorum\Utils\SetUtils;

  //-------------------CHAMP PRIVE-----------------------
  private $_id;
  private $_pseudo;
  private $_mail;
  private $_type;
  private $_passwd;


  //---------------CONSTRUCTEUR---------------------
  /**
  * contructeur prenant 0 ou 4 arguments
  * @param integer$id
  * @param string$pseudo
  * @param string$mail
  * @param int$type
  */
  public function __construct(array $donnees){
    $this->setUtils($donnees);

    }
  //-------------------GETTER-----------------
  /**
  * @return integer$id
  */
  public function getId(){
    return $this->_id;
  }

  /**
  * @return string$pseudo
  */
  public function getPseudo(){
    return $this->_pseudo;
  }

  /**
  * @return string$mail
  */
  public function getMail(){
    return $this->_mail;
  }

  /**
  * @return int$type
  */
  public function getType(){
    return $this->_type;
  }

  /**
  * @return string$passwd
  */
  public function getPasswd(){
    return $this->_passwd;
  }

  //-------------SETTER---------------
  /**
  *@param integer$id
  */
  private function setId($id){
    $this->_id = $id;
  }

  /**
  *@param string$pseudo
  */
  private function setPseudo($pseudo){
    $this->_pseudo = $pseudo;
  }

  /**
  *@param string$mail
  */
  private function setMail($mail){
    $this->_mail = $mail;
  }

  /**
  *@param int$type
  */
  private function setType($type){
    $this->_type = $type;
  }

  /**
  * @param string$passwd
  */
  private function setPasswd($passwd){
    $this->_passwd = $passwd;
  }


}
 ?>
