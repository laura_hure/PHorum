<?php

namespace PHorum\Entity;

//require_once("../Utils/SetUtils.php");

/**
* class representant la table post
*/
class PostEntity{

  use \PHorum\Utils\SetUtils;

  //--------------------CHAMP PRIVE-------------
  private $_id;
  private $_content;
  private $_date;
  private $_sujet;
  private $_utilisateur;

  //----------------CONSTRUCTEUR------------------
  /**
  * @param int$id
  * @param string$content
  * @param int$sujet
  * @param int$utilisateur
  */
  public function  __construct(array $donnees){
      $this->setUtils($donnees);
    }

    //---------------GETTER-----------------
    /**
    * @return int$id
    */
    public function getId(){
      return $this->_id;
    }

    /**
    * @return string$content
    */
    public function getContent(){
      return $this->_content;
    }

    /**
    * @return DateTime$date
    */
    public function getDate(){
      return $this->_date;
    }

    /**
    * @return int$sujet
    */
    public function getSujet(){
      return $this->_sujet;
    }

    /**
    * @return int$utilisateur
    */
    public function getUtilisateur(){
      return $this->_utilisateur;
    }

    //-------------------SETTER---------------
    /**
    * @param int$id
    */
    private function setId($id){
      $this->_id = $id;
    }

    /**
    * @param string$content
    */
    private function setContent($content){
      $this->_content = $content;
    }

    /**
    *@param DateTime$date
    */
    private function setDate($date){
      $this->_date = $date;
    }

    /**
    * @param int$sujet
    */
    private function setSujet($sujet){
      $this->_sujet = $sujet;
    }

    /**
    * @param int$utilisateur
    */
    private function setUtilisateur($utilisateur){
      $this->_utilisateur = $utilisateur;
    }

  }
 ?>
