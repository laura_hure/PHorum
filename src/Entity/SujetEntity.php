<?php
namespace PHorum\Entity;


/**
* Class representant la table sujet
*/
class SujetEntity{

  use \PHorum\Utils\SetUtils;

  //---------------CHAMPS PRIVE------------
  private $_id;
  private $_titre;
  private $_utilisateur;
  private $_categorie;

  //-------------CONSTRUCTEUR--------------
  /**
  * prend en arguments 0 ou 4 parametres
  * @param int$id
  * @param string$titre
  * @param int$utilisateur
  * @param int$categorie
  */
  public function __construct(array $donnees){
    $this->setUtils($donnees);
  }

  //--------------------GETTER--------------
  /**
  * @return int$id
  */
  public function getId(){
    return $this->_id;
  }

  /**
  * @return string$titre
  */
  public function getTitre(){
    return $this->_titre;
  }

  /**
  * @return int$utilisateur
  */
  public function getUtilisateur(){
    return $this->_utilisateur;
  }

  /**
  * @return int$categorie
  */
  public function getCategorie(){
    return $this->_categorie;
  }

  //-----------------------SETTER--------------------
  /**
  * @param int$id
  */
  private function setId($id){
    $this->_id = $id;
  }

  /**
  * @param string$titre
  */
  private function setTitre($titre){
    $this->_titre = $titre;
  }

  /**
  * @param int$utilisateur
  */
  private function setUtilisateur($utilisateur){
    $this->_utilisateur = $utilisateur;
  }

  /**
  * @param int$categorie
  */
  private function setCategorie(int $categorie){
    $this->_categorie = $categorie;
  }
}
 ?>
