<?php

//-------------------------------------------IMPORTS------------------------------
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Debug\ErrorHandler;

use PHorum\Services\InscriptionService;
use PHorum\Services\LoginService;
use PHorum\Services\AdminService;
use PHorum\Services\ForumService;
use PHorum\DAO\UtilisateurDAO;
use PHorum\DAO\CategorieDAO;
use PHorum\DAO\SujetDAO;
use PHorum\DAO\PostDAO;


// web/index.php
require_once('..\vendor\autoload.php');

//--------------------INITIALISATION APP----------------------------
$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => '..\templates',
));

$app->register(new Silex\Provider\SessionServiceProvider());

//$app["debug"]=true;

//ErrorHandler::register();

//-----------------------------------INSCRIPTION USER---------------------------
$app->get('/inscription', function (Request $request) use ($app) {
      //formulaire d'inscription avec ou sans message d'erreur
      return $app["twig"]->render("InscriptionUser.html.twig",array(
        "userSession" => $app["session"]->get("user"),
        "visibility" => $request->get("visibility"),
        "msg_error" => $request->get("msg_error")
      ));
});

$app->post("/inscription",function () use($app){
  if(isset($_POST["pseudo"]) && isset($_POST["email"]) && isset($_POST["passwd"]) && isset($_POST["confirm_passwd"])){
    $pseudo = filter_input(INPUT_POST, "pseudo", FILTER_SANITIZE_SPECIAL_CHARS);
    $msg_error = InscriptionService::CheckFields($pseudo,$_POST["email"],$_POST["passwd"],$_POST["confirm_passwd"]);
    if($msg_error == ""){
      $id = InscriptionService::inscrire($pseudo,$_POST["email"],$_POST["passwd"]);
      return $app->redirect('admin');
    }else{
      //Si un champs n'est pas valide on retourne le formulaire avec un message d'erreur
    $request = Request::create("inscription","GET",array(
      "visibility" => "display:block",
      "msg_error" => $msg_error
    ));
    return $app->handle($request, HttpKernelInterface::SUB_REQUEST);
    }
  }else{
    //Il manque des arguments on redirige vers le formulaire avec un message d'erreur
    $request = Request::create("inscription","GET",array(
      "visibility" => "display:block",
      "msg_error" => "Vous devez renseigner tous les champs."
    ));
    return $app->handle($request, HttpKernelInterface::SUB_REQUEST);
  }

});

//-------------------------------------------LOGIN---------------------------
$app->get("/login",function(Request $request) use($app){
  return $app["twig"]->render("LoginUser.html.twig",array(
    "visibility" => $request->get("visibility"),
    "msg_error" => $request->get("msg_error")
  ));
});

$app->post("/login",function() use($app){
  if(isset($_POST["pseudo"]) && isset($_POST["email"]) && isset($_POST["passwd"])){
    $pseudo = filter_input(INPUT_POST, "pseudo", FILTER_SANITIZE_SPECIAL_CHARS);
    $msg_error = LoginService::CheckFields($pseudo,$_POST["email"],$_POST["passwd"]);
    if($msg_error == ""){
      $user = UtilisateurDAO::getUtilisateurByPseudo($pseudo);
      if($user && password_verify($_POST['passwd'], $user->getPasswd())){
		      // Nous créons la session de l'utilisateur et y inscrivons les informations souhaitées (pseudo, userid et type)
      $app["session"]->start();
		  $app['session']->set('user', array('pseudo' => $user->getPseudo(), 'userid' => $user->getId(), 'type' => $user->getType()));
      //on redirige vers la page de profil de l'utilisateur
      return $app->redirect("accueil");
    }else{
      //message d'erreur indiquant que le mot de passe est incorrecte
      $request = Request::create("login","GET",array(
        "visibility" => "display:block",
        "msg_error" => "Le mot de passe est incorrecte."
      ));
      return $app->handle($request,HttpKernelInterface::SUB_REQUEST);
    }
    }else{
      //message d'erreur indiquant qu'il y a un champ erroné
      $request = Request::create("login","GET",array(
        "visibility" => "display:block",
        "msg_error" => $msg_error
      ));
      return $app->handle($request, HttpKernelInterface::SUB_REQUEST);
    }
  }else{
    //message d'erreur indiquant qu'il faut remplir tout les champs
    $request = Request::create("login","GET",array(
      "visibility" => "display:block",
      "msg_error" => "Vous devez renseigner tous les champs."
    ));
    return $app->handle($request, HttpKernelInterface::SUB_REQUEST);
  }
});


//----------------------------------DECONNEXION----------------------------
$app->get("/deconnexion",function() use($app){
  //on supprime les donnees  de session utilisateur
  $app["session"]->remove("user");
  $app["session"]->clear();
  $app["session"]->invalidate();
  return $app->redirect("accueil");
});

//------------------------------------ADMIN--------------------------------
$app->get("/admin",function(Request $request) use($app){
  //verification que c'est bien l'administrateur qui fait la demande
  if($app["session"]->get("user") && $app["session"]->get("user")["type"] == 1 ){

    //suppression d'un utilisateur
    if($request->get("delete_user")){
      UtilisateurDAO::deleteUtilisateurById(intVal($request->get("delete_user")));
    }

    //suppression d'une categorie et des posts et sujets associés
    if($request->get("delete_categorie")){
        AdminService::deleteCategorie(intVal($request->get("delete_categorie")));
    }

    //telechargement des sujets et posts d'une categories
    if($request->get("download_categorie")){
      AdminService::downloadCategorie(intVal($request->get("download_categorie")));
    }

    if($request->get("query") && $request->get("query") == "categories"){
      //si on demande la categorie on renvois sur la page d'admin sur l'onglet categories
      return $app["twig"]->render("Admin.html.twig",array(
        "userSession" => $app["session"]->get("user"),
        "categories" => CategorieDAO::getAllCategories(),
        "erreur" =>  $request->get("erreur"),
        "success" => $request->get("success")
      ));
    }

    //par defaut on dirige sur l'onglet membres de l'admin
    return $app["twig"]->render("Admin.html.twig",array(
        "userSession" => $app["session"]->get("user"),
        "utilisateurs" => UtilisateurDAO::getAllUtilisateurs(),
        "erreur" =>  $request->get("erreur"),
        "success" => $request->get("success")
      ));

    }else{
      /*si l'utilisateur n'est pas authentifier en tant qu'administrateur on
      lui affiche un message lui demandant de s'authentifier en tant qu'administrateur
      pour acceder a la page*/
      return $app["twig"]->render("Admin.html.twig",array(
        "userSession" =>$app["session"]->get("user")
      ));
    }
});

$app->post("/admin",function(Request $request) use($app){
  //On vérifie si les données viennent bien de l'administrateur
  if($app["session"]->get("user") && $app["session"]->get("user")["type"] == 1){
    if($request->get("update_categorie")){
      CategorieDAO::updateCategorieTitre(intVal($request->get("update_categorie")),$_POST["categorie"]);

      $redirect = Request::create("admin","GET",array(
        "query" => "categories",
      ));
      return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
    }
    if($request->get("add_categorie")){
      $categorie_id = CategorieDAO::createCategorie($_POST["categorie"]);

      $redirect = Request::create("admin","GET",array(
        "query" => "categories"
      ));
      return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
    }
  }
});

//------------------------------------SUJETS--------------------------------
$app->get("sujets/{categorie}",function(Request $request, $categorie) use($app){

  if($app["session"]->get("user") && $app["session"]->get("user")["type"] == 1 && $request->get("delete_sujet")){
    ForumService::deleteSujetAndPosts(intVal($request->get("delete_sujet")));
  }

  //si un utilisateur est connecte on lui customise l'affichage des sujets
  if($app["session"]->get("user")){
    $sujets = SujetDAO::getSujetsByCategorieSansUser(intVal($categorie),intVal($app["session"]->get("user")["userid"]));
    return $app["twig"]->render("Sujets.html.twig",array(
      "userSession" => $app["session"]->get("user"),
      "sujets" => $sujets,
      "categorie" => $categorie,
      "userSujets" => SujetDAO::getSujetByUtilisateurAndCategorie(intVal($categorie),intVal($app["session"]->get("user")["userid"])),
      "users" => ForumService::getUserForSujet($sujets)
    ));
  }

  //par defaut on affiche juste la liste des sujets pour cette categorie
  $sujets = SujetDAO::getSujetsByCategorie(intVal($categorie));
  return $app["twig"]->render("Sujets.html.twig",array(
    "sujets" => $sujets,
    "users" => ForumService::getUserForSujet($sujets)
  ));
});

$app->post("sujets/{categorie}/ajouter",function($categorie, Request $request) use($app){
  if($request->get("addFor")){
    //on vérifie l'identité de l'utilisateur
    if($app["session"]->get("user") && $app["session"]->get("user")["pseudo"] == UtilisateurDAO::getUtilisateurById(intVal($request->get("addFor")))->getPseudo()){
      if(isset($_POST["sujet"])){
        $sujet = filter_input(INPUT_POST, "sujet", FILTER_SANITIZE_SPECIAL_CHARS);
        if(ForumService::checkSujet($sujet)){
          $sujet_id = SujetDAO::createSujet($sujet,intVal($request->get("addFor")),intVal($categorie));
        }
      }
    }
  }
  return $app->redirect("/PHorum/web/index.php/sujets/".$categorie);
});

//-----------------------------POSTS---------------------------------------
$app->get("posts/{sujet}",function($sujet, Request $request)use($app){
  if($app["session"]->get("user")
  && $request->get("delete_post")
  && ($app["session"]->get("user")["type"] == 1 || $app["session"]->get("user")["userid"] == PostDAO::getPostById((intVal($request->get("delete_post"))))->getUtilisateur())
  ){
    PostDAO::deletePostById(intVal($request->get("delete_post")));
  }

  $posts = PostDAO::getPostsBySujet(intVal($sujet));
  return $app["twig"]->render("posts.html.twig",array(
    "userSession" => $app["session"]->get("user"),
    "sujet" => $sujet,
    "posts" => $posts,
    "users" => ForumService::getUserForPost($posts)
  ));
});

$app->post("posts",function(Request $request)use($app){
  if($request->get("addFor") && $request->get("sujet")){
    if($app["session"]->get("user") && $app["session"]->get("user")["pseudo"] == UtilisateurDAO::getUtilisateurById(intVal($request->get("addFor")))->getPseudo()){
      if(isset($_POST["post"])){
        $post_id = PostDAO::createPost(intVal($request->get("addFor")),intVal($request->get("sujet")),$_POST["post"]);
      }
    }
  }
  return $app->redirect("/PHorum/web/index.php/posts/".$request->get("sujet"));
});

//------------------------------------ACCEUIL------------------------------
$app->get("accueil",function()use($app){
  return $app["twig"]->render("Accueil.html.twig",array(
    "userSession" => $app["session"]->get("user"),
    "categories" => CategorieDAO::getAllCategories()
  ));
});

//----------------------------------PROFILE--------------------------------
$app->get("profile",function(Request $request)use($app){

  //si un utilisateur est connecté on lui affiche son profile
  if($app["session"]->get("user")){

    if(ForumService::imageExist("C:\\xampp\\htdocs\\PHorum\\resources\\images\\".$app["session"]->get("user")["pseudo"].".jpg")){ //si l'image renseigne le lien
      return $app["twig"]->render("Profile.html.twig",array(
        "userSession" => $app["session"]->get("user"),
        "utilisateur" => UtilisateurDAO::getUtilisateurById($app["session"]->get("user")["userid"]),
        "image" => "http://localhost/PHorum/resources/images/".$app["session"]->get("user")["pseudo"].".jpg",
        "erreur" => $request->get("erreur"),
      ));
    }else{//si l'image n'existe pas on affiche celle par défaut
      return $app["twig"]->render("Profile.html.twig",array(
        "userSession" => $app["session"]->get("user"),
        "utilisateur" => UtilisateurDAO::getUtilisateurById($app["session"]->get("user")["userid"]),
        "erreur" => $request->get("erreur"),
      ));
    }
  }else{
    return $app["twig"]->render("Profile.html.twig");
  }
});

$app->post("profile",function()use($app){
  //on vverifie juste que c'est un utilisateur connecté qui fait la demande
  if($app["session"]->get("user")){
    //on verifie que l'image est bien renseigné
    $erreur_message = ForumService::uploadImage($_FILES["image"],$app["session"]->get("user")["pseudo"]);
  }
  if($erreur_message == ""){
    return $app->redirect("profile");

  }else{
    //si il y a eu une erreur on retourne la cause de cette erreur pour informer l'utilisateur
    $redirect = Request::create("profile","GET",array(
      "erreur" => $erreur_message
    ));
    return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
  }

});

//-------------------------------------IMPORT-------------------------------
$app->post("import",function()use($app){
  //on verifie que l'utilisateur est bien connecté et est l'admin
  if($app["session"]->get("user") && $app["session"]->get("user")["type"] == 1){
    try{
      $erreur_message = AdminService::import($_FILES["import"]);
      if($erreur_message != ""){
        $redirect = Request::create("admin","GET",array(
          "erreur" => $erreur_message
        ));
        return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
      }
    }catch(Exception $e){
      $redirect = Request::create("admin","GET",array(
        "erreur" => "Une erreur s'est produite lors de l'import. Veuillez vérifier la conformité du document."
      ));
      return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
    }
  }
  $redirect = Request::create("admin","GET",array(
    "success" => "L'import c'est effectué sans problèmes."
  ));
  return $app->handle($redirect, HttpKernelInterface::SUB_REQUEST);
});

//-------------------------------------ERREUR------------------------------
/*$app->error(function (\Exception $e, Request $request, $code) use($app) {
  if ($app['debug']) {
        return;
    }
    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }

    return new Response($message);
});*/

$app->run();
